﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Pickup))]
public class PickupEditor : Editor 
{
    Pickup pickup;

    void Awake()
    {
        pickup = (Pickup)target;
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();        

        SerializedProperty pickupType = serializedObject.FindProperty("pickupType");
        SerializedProperty manipPickup = serializedObject.FindProperty("manipulator");
        SerializedProperty keyColor = serializedObject.FindProperty("keyColor");
        SerializedProperty healAmount = serializedObject.FindProperty("healAmount");

        EditorGUILayout.PropertyField(pickupType);

        switch ((PickupType)pickupType.enumValueIndex)
        {
            // Only show healing amount for healing pickups
            case(PickupType.HEALTH_INST):
            case(PickupType.HEALTH_PERM):
            case(PickupType.MANA_INST):
            case(PickupType.MANA_PERM):
                EditorGUILayout.PropertyField(healAmount);
                break;
            // Only show key type for key pickups
            case(PickupType.KEY_INST):
            case (PickupType.KEY_MASTR):
                EditorGUILayout.PropertyField(keyColor);
                break;
            // Only show manipulator type for manipulator pickups
            case(PickupType.MANIPULATOR):
                // Check if manipulator is NONE.
                //    If it is, set GUI color to red
                //    Otherwise, set GUI color to white
                if (manipPickup.enumValueIndex == (int)Manipulator.NONE)
                    GUI.color = Color.red;
                else
                    GUI.color = Color.white;

                EditorGUILayout.PropertyField(manipPickup);

                // Let designer know manipulator needs to be valid
                if (manipPickup.enumValueIndex == (int)Manipulator.NONE)
                {
                    EditorGUILayout.LabelField("Manipulator needs to be set to a valid value!");
                }
                break;
            default:
                break;
        }

        serializedObject.ApplyModifiedProperties();
    }
}
