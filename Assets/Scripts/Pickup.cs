﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class Pickup : MonoBehaviour
{
    public PickupType pickupType;
    public Manipulator manipulator;
    public KeyColor keyColor;
    public int healAmount;

    BoxCollider2D pickupCollider;
    GameObject player;

    void Awake()
    {
        pickupCollider = GetComponent<BoxCollider2D>();
        pickupCollider.isTrigger = true;
        player = GameObject.Find("Player");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        
        switch(pickupType)
        {
            case (PickupType.HEALTH_INST):            
                player.GetComponent<PlayerController>().AddHealth(healAmount, true);
                break;
            case (PickupType.HEALTH_PERM):
                player.GetComponent<PlayerController>().AddHealth(healAmount, false);
                break;
            case (PickupType.MANA_INST):
                player.GetComponent<PlayerController>().AddMana(healAmount, true);
                break;
            case (PickupType.MANA_PERM):
                player.GetComponent<PlayerController>().AddMana(healAmount, false);
                break;
            case (PickupType.ABILITY_DOUBLE):
                player.GetComponent<PlayerPhysics2D>().AcquireDoubleJump();
                break;
            case (PickupType.ABILITY_CLIMB):
                player.GetComponent<PlayerPhysics2D>().AcquireWallCling();
                break;
            case(PickupType.KEY_INST):
                switch (keyColor)
                {
                    case(KeyColor.RED):
                        player.GetComponent<PlayerController>().AddKey(KeyColor.RED, true);
                        break;
                    case(KeyColor.BLUE):
                        player.GetComponent<PlayerController>().AddKey(KeyColor.BLUE, true);
                        break;
                    case(KeyColor.GOLD):
                        player.GetComponent<PlayerController>().AddKey(KeyColor.GOLD, true);
                        break;
                }
                break;
            case (PickupType.KEY_MASTR):
                switch (keyColor)
                {
                    case (KeyColor.RED):
                        player.GetComponent<PlayerController>().GetMasterKey(KeyColor.RED);
                        break;
                    case (KeyColor.BLUE):
                        player.GetComponent<PlayerController>().GetMasterKey(KeyColor.BLUE);
                        break;
                    case (KeyColor.GOLD):
                        player.GetComponent<PlayerController>().GetMasterKey(KeyColor.GOLD);
                        break;
                }
                break;
            case (PickupType.MANIPULATOR):
                switch(manipulator)
                {
                    case (Manipulator.HEAT):
                        player.GetComponent<Manipulators>().AddManipulator(Manipulator.HEAT);
                        break;
                    case (Manipulator.CHILL):
                        player.GetComponent<Manipulators>().AddManipulator(Manipulator.CHILL);
                        break;
                    case (Manipulator.HARDEN):
                        player.GetComponent<Manipulators>().AddManipulator(Manipulator.HARDEN);
                        break;
                    case (Manipulator.SOFTEN):
                        player.GetComponent<Manipulators>().AddManipulator(Manipulator.SOFTEN);
                        break;
                }
                break;
        }
        Destroy(gameObject);
    }

    void OnTriggerStay2D(Collider2D other)
    {

    }

    void ApplyPickup()
    {

    }
}
