﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReticleBehaviour : MonoBehaviour 
{
    public GameObject player;
    [Tooltip("Max distance out from the player the target reticle can go out.")]
    public float distance;

    
    Vector3 mousePos;

    bool confineMouse;

    public void Start()
    {
        confineMouse = true;        
        player = GameObject.Find(Constants.PLAYER);
        InvokeRepeating("OutputMousePosition", 1, 1);
        UpdateMousePosition();
    }

    void Update()
    {
        UpdateMousePosition();

        if (!player.GetComponent<PlayerAttack>().throwingChakram)
        {            
            transform.position = Vector3.Lerp(transform.position, player.transform.position + Vector3.ClampMagnitude(mousePos, distance), 1);
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            confineMouse = !confineMouse;

        if (confineMouse)
        {
            Cursor.lockState = CursorLockMode.Confined;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    void UpdateMousePosition()
    {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePos.z = 0;
    }

    void OutputMousePosition()
    {
        Debug.Log("Mouse is at " + Camera.main.ScreenToWorldPoint(Input.mousePosition));
    }
}
