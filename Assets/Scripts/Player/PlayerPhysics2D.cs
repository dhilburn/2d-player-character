﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPhysics2D : MonoBehaviour
{
    [Tooltip("How fast the player moves left and right.")]
    public float moveSpeed;
    [Tooltip("Maximum speed the player can move. Affects ALL movement")]
    public float maxMoveSpeed;
    [HideInInspector] public float maxInclineClimb;
    [Tooltip("How high player jumps from ground")]
    public float jumpForce;
    [Tooltip("How high player jumps in the air")]
    public float doubleJumpForce;
    [Tooltip("How quickly the player can climb up walls")]
    public float wallClimbSpeed;
    [Tooltip("Drag applied to the player to make them stop moving on the ground.")]
    public float stopMovingDrag;
    [Tooltip("Drag applied to the player when they are clinging to a wall.")]
    public float wallClingDrag;
    [Tooltip("Show player collision rays. Use for debugging")]
    public bool showCollisionRays = false;

    bool hasDouble;
    bool hasWallClimb;

    float currentPlatformIncline;

    //public float editableGravity;
    //[SerializeField] float gravity;

    [HideInInspector] public bool facingRight;
    bool grounded;
    bool crouching;
    bool canJump;
    bool canDoubleJump;
    bool wallSliding;

    Rigidbody2D playerRigidBody;
    BoxCollider2D playerCollider;

    PlayerController playerController;
    float gravity;
    
    [HideInInspector] public GameObject[] leftCollisionPoints;
    [HideInInspector] public GameObject[] rightCollisionPoints;
    [HideInInspector] public GameObject[] topCollisionPoints;
    [HideInInspector] public GameObject[] bottomCollisionPoints;
    [HideInInspector] public GameObject leftCornerPoint;
    [HideInInspector] public GameObject rightCornerPoint;

    /// <summary>
    /// Awake sets the references to the player object's collider and rigidbody
    /// </summary>
    void Awake()
    {
        Application.targetFrameRate = 60;
        playerRigidBody = GetComponent<Rigidbody2D>();
        playerCollider = GetComponent<BoxCollider2D>();
        playerController = GetComponent<PlayerController>();
        InitializeCollisionPoints();
        gravity = playerRigidBody.gravityScale;
    }

    void Start()
    {
        
    }

    #region Unity Update Functions
    void FixedUpdate()
    {
        // Constrain maximum velocity
        playerRigidBody.velocity = Vector3.ClampMagnitude(playerRigidBody.velocity, maxMoveSpeed);

        // If we are on the floor, gravity remains as is
        if (CheckFloorCollision())
        {
            playerRigidBody.gravityScale = gravity;
        }
        // If we ae on an incline, set gravity to 0 so we don't slide
        else if (CheckCornerCollisionLeftDown() || CheckCornerCollisionRightDown() || CheckCornerCollisionLeftForward() || CheckCornerCollisionRightForward())
        {
            playerRigidBody.gravityScale = 0;
        }

        // Check if we are colliding with either the floor or a ramp
        if (CheckFloorCollision() || CheckCornerCollisionLeftDown() || CheckCornerCollisionRightDown())
        {
            
            // Set drag and appropriate flags
            playerRigidBody.drag = stopMovingDrag;
            grounded = true;
            canJump = true;
            if (hasDouble)
                canDoubleJump = true;
        }
        // Check if we have the wall slide and are colliding with a wall in the air
        else if (hasWallClimb && (CheckUngroundedWallCollisionRight() || CheckUngroundedWallCollisionLeft()))
        {
            playerRigidBody.gravityScale = gravity;
            playerRigidBody.drag = wallClingDrag;
            wallSliding = true;
        }
        // We aren't colliding with anything
        else
        {
            playerRigidBody.gravityScale = gravity;
            playerRigidBody.drag = 0;
            grounded = false;
            canJump = false;
            wallSliding = false;
        }
    }

    /// <summary>
    /// Update constantly updates the corners of the player character
    /// </summary>
    void Update()
    {
        UpdateCollisionPoints();
        if (showCollisionRays)
            DrawCollisionRays();
    }

    void LateUpdate()
    {
        #region Movement

        #region Move Right

        #region On Ground

        // Moving right on the ground
        if (RightMovementKeyDown() && !CheckWallCollisionRight() && !GetComponent<PlayerAttack>().throwingChakram)
        {
            // Walking on a flat plain
            if (CheckFloorCollision())
            {
                currentPlatformIncline = 0;
                MoveRight();
            }
            // Walking right DOWN a ramp
            else
            {
                if (CheckCornerCollisionLeftDown() || CheckCornerCollisionLeftForward())
                {
                    playerRigidBody.drag = stopMovingDrag;
                    RaycastHit2D hit = Physics2D.Raycast(leftCornerPoint.transform.position, Vector3.left, Constants.SIDE_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT));
                    currentPlatformIncline = Vector2.Angle(hit.normal, Vector3.up);
                    MoveRightInclineDown(currentPlatformIncline);
                }
                // Walking right UP a ramp
                else if (CheckCornerCollisionRightDown() || CheckCornerCollisionRightForward())
                {
                    playerRigidBody.drag = stopMovingDrag;
                    RaycastHit2D hit = Physics2D.Raycast(rightCornerPoint.transform.position, Vector3.right, Constants.SIDE_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT));
                    currentPlatformIncline = Vector2.Angle(hit.normal, Vector3.up);
                    MoveRightInclineUp(currentPlatformIncline);
                }
            }
        }

        #endregion

        #region In Air
        if (RightMovementKeyDown() && !CheckWallCollisionRight() && !CheckUngroundedWallCollisionRight() && !GetComponent<PlayerAttack>().throwingChakram)
            MoveRight();
        #endregion

        #endregion

        #region Move Left

        #region On Ground
        if (LeftMovementKeyDown() && !CheckWallCollisionLeft() && !GetComponent<PlayerAttack>().throwingChakram)
        {
            if (CheckFloorCollision())
            {
                currentPlatformIncline = 0;
                MoveLeft();
            }
            else
            {
                if (CheckCornerCollisionLeftForward())
                {
                    playerRigidBody.drag = stopMovingDrag;
                    RaycastHit2D hit = Physics2D.Raycast(leftCornerPoint.transform.position, Vector3.left, Constants.SIDE_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT));
                    currentPlatformIncline = Vector2.Angle(hit.normal, Vector3.up);
                    MoveLeftInclineUp(currentPlatformIncline);
                }
                else if (CheckCornerCollisionRightForward())
                {
                    playerRigidBody.drag = stopMovingDrag;
                    RaycastHit2D hit = Physics2D.Raycast(rightCornerPoint.transform.position, Vector3.right, Constants.SIDE_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT));
                    currentPlatformIncline = Vector2.Angle(hit.normal, Vector3.up);
                    MoveLeftInclineDown(currentPlatformIncline);
                }
            }
        }
        #endregion

        #region In Air
        if (LeftMovementKeyDown() && !CheckWallCollisionLeft() && !CheckUngroundedWallCollisionLeft() && !GetComponent<PlayerAttack>().throwingChakram)
            MoveLeft();
        #endregion

        #endregion

        #region Jump
        if (JumpKeyPressed() && crouching && grounded)
        {
            Crouch();
        }        
        else
        {
            if (canJump)
            {
                if (JumpKeyPressed() && !GetComponent<PlayerAttack>().throwingChakram)
                {
                    Jump();
                    canJump = false;
                }
            }

            if (canDoubleJump && !CheckFloorCollision() && !GetComponent<PlayerAttack>().throwingChakram)
            {
                if (JumpKeyPressed())
                {
                    DoubleJump();
                    canDoubleJump = false;
                }
            }
        }
        #endregion

        if (CrouchingKeyPressed() && grounded)
        {
            Crouch();
        }

        if (ClimbKeyDown() && hasWallClimb && (CheckWallCollisionRight() || CheckWallCollisionLeft()))
        {
            WallClimb();
        }
        #endregion
    }
    #endregion

    #region Unity Collision functions
    void OnCollisionEnter2D(Collision2D collision)
    {
        
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        
    }
    #endregion

    #region Collision Point management functions

    #region Initialize Collision Points
    void InitializeCollisionPoints()
    {
        InitializeLeftCollisionPoints();
        InitializeRightCollisionPoints();
        InitializeTopCollisionPoints();
        InitializeBottomCollisionPoints();
        InitializeCornerPoints();
    }

    void InitializeBottomCollisionPoints()
    {
        bottomCollisionPoints = new GameObject[9];
        for (int i = 0; i < 9; i++)
            bottomCollisionPoints[i] = new GameObject();

        UpdateBottomPoints();

        foreach (GameObject collisionPoint in bottomCollisionPoints)
        {
            collisionPoint.transform.parent = transform;
            collisionPoint.gameObject.name = Constants.COLLISION_POINT + " - Bottom";
            collisionPoint.gameObject.tag = Constants.COLLISION_POINT;
            collisionPoint.gameObject.layer = LayerMask.NameToLayer(Constants.PLAYER);
        }
    }

    void InitializeLeftCollisionPoints()
    {
        leftCollisionPoints = new GameObject[10];
        for (int i = 0; i < 10; i++)
            leftCollisionPoints[i] = new GameObject();

        UpdateLeftPoints();

        foreach(GameObject collisionPoint in leftCollisionPoints)
        {
            collisionPoint.transform.parent = transform;
            collisionPoint.gameObject.name = Constants.COLLISION_POINT + " - Left Side";
            collisionPoint.gameObject.tag = Constants.COLLISION_POINT;
            collisionPoint.gameObject.layer = LayerMask.NameToLayer(Constants.PLAYER);
        }
    }

    void InitializeRightCollisionPoints()
    {
        rightCollisionPoints = new GameObject[10];
        for (int i = 0; i < 10; i++)
            rightCollisionPoints[i] = new GameObject();

        UpdateRightPoints();

        foreach (GameObject collisionPoint in rightCollisionPoints)
        {
            collisionPoint.transform.parent = transform;
            collisionPoint.gameObject.name = Constants.COLLISION_POINT + " - Right Side";
            collisionPoint.gameObject.tag = Constants.COLLISION_POINT;
            collisionPoint.gameObject.layer = LayerMask.NameToLayer(Constants.PLAYER);
        }
    }

    void InitializeTopCollisionPoints()
    {
        topCollisionPoints = new GameObject[11];
        for (int i = 0; i < 11; i++)
            topCollisionPoints[i] = new GameObject();

        UpdateTopPoints();

        foreach (GameObject collisionPoint in topCollisionPoints)
        {
            collisionPoint.transform.parent = transform;
            collisionPoint.gameObject.name = Constants.COLLISION_POINT + " - Top";
            collisionPoint.gameObject.tag = Constants.COLLISION_POINT;
            collisionPoint.gameObject.layer = LayerMask.NameToLayer(Constants.PLAYER);
        }
    }    

    void InitializeCornerPoints()
    {
        leftCornerPoint = new GameObject();
        rightCornerPoint = new GameObject();
        
        UpdateCorners();

        leftCornerPoint.transform.parent = transform;
        leftCornerPoint.gameObject.name = Constants.COLLISION_POINT + " - Left Corner";
        leftCornerPoint.gameObject.tag = Constants.COLLISION_POINT;
        leftCornerPoint.gameObject.layer = LayerMask.NameToLayer(Constants.PLAYER);

        rightCornerPoint.transform.parent = transform;
        rightCornerPoint.gameObject.name = Constants.COLLISION_POINT + " - Right Corner";
        rightCornerPoint.gameObject.tag = Constants.COLLISION_POINT;
        rightCornerPoint.gameObject.layer = LayerMask.NameToLayer(Constants.PLAYER);
        
    }
    #endregion

    #region Update Collision Points
    void UpdateCollisionPoints()
    {
        UpdateLeftPoints();
        UpdateRightPoints();
        UpdateTopPoints();
        UpdateBottomPoints();
        UpdateCorners();
    }

    void UpdateBottomPoints()
    {
        float initialX = playerCollider.bounds.min.x;
        float finalX = playerCollider.bounds.max.x;
        float magnitude = playerCollider.bounds.size.x;

        for (int i = 0; i < 9; i++)
            bottomCollisionPoints[i].transform.position = new Vector3(initialX + (magnitude * (0.1f * (i + 1))), playerCollider.bounds.min.y);
    }

    void UpdateLeftPoints()
    {
        // First Y position to check
        float initialY = playerCollider.bounds.min.y;
        // Final Y position to check
        float finalY = playerCollider.bounds.max.y;
        // Distance between first and last Y
        float magnitude = playerCollider.bounds.size.y;


        // Add points between first and last point to collision points array
        for (int i = 0; i < 9; i++)
            leftCollisionPoints[i].transform.position = new Vector3(playerCollider.bounds.min.x, initialY + (magnitude * (0.1f * (i + 1))));

        // Add last point to collision points array
        leftCollisionPoints[9].transform.position = new Vector3(playerCollider.bounds.min.x, finalY);
    }

    void UpdateRightPoints()
    {
        float initialY = playerCollider.bounds.min.y;
        float finalY = playerCollider.bounds.max.y;
        float magnitude = playerCollider.bounds.size.y;

        for (int i = 0; i < 9; i++)
            rightCollisionPoints[i].transform.position = new Vector3(playerCollider.bounds.max.x, initialY + (magnitude * (0.1f * (i + 1))));
        rightCollisionPoints[9].transform.position = new Vector3(playerCollider.bounds.max.x, finalY);
    }

    void UpdateTopPoints()
    {
        float initialX = playerCollider.bounds.min.x;
        float finalX = playerCollider.bounds.max.x;
        float magnitude = playerCollider.bounds.size.x;

        topCollisionPoints[0].transform.position = new Vector3(initialX, playerCollider.bounds.max.y);
        for (int i = 1; i < 10; i++)
            topCollisionPoints[i].transform.position = new Vector3(initialX + (magnitude * (0.1f * i)), playerCollider.bounds.max.y);
        topCollisionPoints[10].transform.position = new Vector3(finalX, playerCollider.bounds.max.y);
    }    

    void UpdateCorners()
    {
        float leftX = playerCollider.bounds.min.x;
        float rightX = playerCollider.bounds.max.x;
        float cornersY = playerCollider.bounds.min.y;

        leftCornerPoint.transform.position = new Vector3(leftX, cornersY);
        rightCornerPoint.transform.position = new Vector3(rightX, cornersY);        
    }

    #endregion

    #endregion

    #region Collision Detection functions

    bool CheckFloorCollision()
    {
        bool colliding = false;

        for (int i = 0; i < bottomCollisionPoints.Length; i++)
        {
            if (Physics2D.Raycast(bottomCollisionPoints[i].transform.position, Vector3.down, Constants.TOP_BOTTOM_COLLISION_SCALE, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
            {
                colliding = true;
                break;
            }
        }

        return colliding;
    }

    bool CheckWallCollisionLeft()
    {
        bool colliding = false;        

        for(int i = 0; i < leftCollisionPoints.Length; i++)
        {
            // Check if you are colliding with a wall based on left side points
            if(Physics2D.Raycast(leftCollisionPoints[i].transform.position, Vector3.left, Constants.SIDE_COLLISION_SCALE, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
            {
                colliding = true;
                break;
            }
        }

        return colliding;
    }

    bool CheckWallCollisionRight()
    {
        bool colliding = false;

        for (int i = 0; i < rightCollisionPoints.Length; i++)
        {
            if (Physics2D.Raycast(rightCollisionPoints[i].transform.position, Vector3.right, Constants.SIDE_COLLISION_SCALE, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
            {
                colliding = true;
                break;
            }
        }

        return colliding;
    }

    bool CheckUngroundedWallCollisionLeft()
    {
        bool colliding = false;

        // Check if not colliding with the floor or an incline
        if(!CheckFloorCollision() && !CheckCornerCollisionLeftDown() && !CheckCornerCollisionRightDown())
        {
            for(int i = 0; i < leftCollisionPoints.Length; i++)
            {
                // Check if colliding with a right side collision point
                if(Physics2D.Raycast(leftCollisionPoints[i].transform.position, Vector3.left, Constants.SIDE_COLLISION_SCALE, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
                {
                    colliding = true;
                    break;
                }
            }
            // If no right side collision point detected, check if there is a collision with the corner point
            if(!colliding)
                if(CheckCornerCollisionLeftForward())
                    colliding = true;
        }

        return colliding;
    }    

    bool CheckUngroundedWallCollisionRight()
    {
        bool colliding = false;

        if (!CheckFloorCollision() && !CheckCornerCollisionLeftDown() && !CheckCornerCollisionRightDown())
        {
            for (int i = 0; i < rightCollisionPoints.Length; i++)
            {
                if (Physics2D.Raycast(rightCollisionPoints[i].transform.position, Vector3.right, Constants.SIDE_COLLISION_SCALE, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
                {
                    colliding = true;
                    break;
                }
                if(!colliding)
                    if (CheckCornerCollisionRightForward())
                        colliding = true;
            }
        }

        return colliding;
    }

    bool CheckCornerCollisionLeftDown()
    {
        return Physics2D.Raycast(leftCornerPoint.transform.position, Vector3.down, Constants.TOP_BOTTOM_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null;
    }

    bool CheckCornerCollisionRightDown()
    {
        return Physics2D.Raycast(rightCornerPoint.transform.position, Vector3.down, Constants.TOP_BOTTOM_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null;
    }

    bool CheckCornerCollisionLeftForward()
    {
        return Physics2D.Raycast(leftCornerPoint.transform.position, Vector3.left, Constants.SIDE_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null;
    }

    bool CheckCornerCollisionRightForward()
    {
        return Physics2D.Raycast(rightCornerPoint.transform.position, Vector3.right, Constants.SIDE_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null;
    }

    #endregion

    #region Movement Functions
    
    #region Keys
    bool RightMovementKeyDown()
    {
        
        return Input.GetKey(KeyCode.D)
            || Input.GetKey(KeyCode.RightArrow);
    }

    bool LeftMovementKeyDown()
    {
        return Input.GetKey(KeyCode.A)
            || Input.GetKey(KeyCode.LeftArrow);
    }

    bool ClimbKeyDown()
    {
        return Input.GetKey(KeyCode.W)
            || Input.GetKey(KeyCode.UpArrow);
    }

    bool JumpKeyPressed()
    {
        return Input.GetButtonDown("Jump");
    }

    bool CrouchingKeyPressed()
    {
        return Input.GetKeyDown(KeyCode.S)
            || Input.GetKeyDown(KeyCode.DownArrow);
    }
    #endregion

    #region Movements
    void MoveRight()
    {
        facingRight = true;
        playerRigidBody.velocity = new Vector2(moveSpeed, playerRigidBody.velocity.y);
    }

    void MoveLeft()
    {
        facingRight = false;
        playerRigidBody.velocity = new Vector2(-moveSpeed, playerRigidBody.velocity.y);
    }

    void MoveRightInclineUp(float inclineAngle)
    {
        facingRight = true;
        if (inclineAngle > 0 && inclineAngle < 80)
        {
            playerRigidBody.velocity = new Vector2(moveSpeed * Mathf.Cos(inclineAngle * Mathf.Deg2Rad), moveSpeed * Mathf.Sin(inclineAngle * Mathf.Deg2Rad));
        }
    }

    void MoveLeftInclineUp(float inclineAngle)
    {
        facingRight = false;
        if (inclineAngle > 0 && inclineAngle < 80)
        {
            playerRigidBody.velocity = new Vector2(-moveSpeed * Mathf.Cos(inclineAngle * Mathf.Deg2Rad), moveSpeed * Mathf.Sin(inclineAngle * Mathf.Deg2Rad));
        }
    }

    void MoveRightInclineDown(float inclineAngle)
    {
        facingRight = true;
        if (inclineAngle > 0 && inclineAngle < 80)
        {
            playerRigidBody.velocity = new Vector2(moveSpeed * Mathf.Cos(inclineAngle * Mathf.Deg2Rad), -moveSpeed * Mathf.Sin(inclineAngle * Mathf.Deg2Rad));
        }
    }

    void MoveLeftInclineDown(float inclineAngle)
    {
        facingRight = false;
        if (inclineAngle > 0 && inclineAngle < 80)
        {
            playerRigidBody.velocity = new Vector2(-moveSpeed * Mathf.Cos(inclineAngle * Mathf.Deg2Rad), -moveSpeed * Mathf.Sin(inclineAngle * Mathf.Deg2Rad));
        }
    }

    void Jump()
    {
        playerRigidBody.gravityScale = gravity;
        playerRigidBody.velocity = new Vector3(playerRigidBody.velocity.x, jumpForce);
    }

    void DoubleJump()
    {
        playerRigidBody.velocity = new Vector3(playerRigidBody.velocity.x, doubleJumpForce);
    }

    void WallClimb()
    {
        playerRigidBody.velocity = new Vector2(0, wallClimbSpeed);
    }

    void Crouch()
    {
        float newScale = 1;
        if (!crouching)
        {
            newScale = transform.localScale.y / 2;
            transform.position = new Vector3(transform.position.x, transform.position.y - transform.localScale.y / 4);
        }
        else
        {
            newScale = transform.localScale.y * 2;
            transform.position = new Vector3(transform.position.x, transform.position.y + transform.localScale.y / 4);
        }
        transform.localScale = new Vector3(transform.localScale.x, newScale);
        crouching = !crouching;
    }

    void LateralMovement()
    {

    }
    #endregion

    #endregion

    public void AcquireDoubleJump()
    {
        hasDouble = true;
    }

    public void AcquireWallCling()
    {
        hasWallClimb = true;
    }

    #region Debug

    void CheckGroundCollisions()
    {
        if (CheckFloorCollision())
            Debug.Log("I am grounded.");

        if (Physics2D.Raycast(leftCornerPoint.transform.position, Vector3.down, Constants.TOP_BOTTOM_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
            Debug.Log("My left corner is colliding with the ground");
        if (Physics2D.Raycast(leftCornerPoint.transform.position, Vector3.left, Constants.SIDE_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
            Debug.Log("My left corner is colliding with a wall");
        if (Physics2D.Raycast(leftCornerPoint.transform.position, Vector3.down + Vector3.left, Constants.TOP_BOTTOM_COLLISION_SCALE * Mathf.Sqrt(2), 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
            Debug.Log("My left corner is detecting a collision straight out");

        if (Physics2D.Raycast(rightCornerPoint.transform.position, Vector3.down, Constants.TOP_BOTTOM_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
            Debug.Log("My right corner is colliding with the ground");
        if (Physics2D.Raycast(rightCornerPoint.transform.position, Vector3.right, Constants.SIDE_COLLISION_SCALE * 1.5f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
            Debug.Log("My right corner is colliding with a wall");
        if (Physics2D.Raycast(rightCornerPoint.transform.position, Vector3.down + Vector3.right, Constants.TOP_BOTTOM_COLLISION_SCALE * Mathf.Sqrt(2), 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null)
            Debug.Log("My right corner is detecting a collision straight out");

    }

    private void OnGUI()
    {
        Rect debugInfoRect = new Rect(0, 0, 200, 250);
        Rect timeRect = new Rect(Screen.width - Screen.width / 4, Screen.height - 40, Screen.width / 4, 40);

        GUILayout.BeginArea(debugInfoRect);
            GUI.color = Color.black;
            GUILayout.Label("Facing " + (facingRight ? "right" : "left"));
            GUILayout.Label("Gravity: " + playerRigidBody.gravityScale);
            GUILayout.Label("Drag: " + playerRigidBody.drag);
            GUILayout.Label("Grounded? " + (grounded ? "yes" : "no"));
            GUILayout.Label("Can Jump? " + (canJump ? "yes" : "no"));
            GUILayout.Label("Can Double Jump? " + (canDoubleJump ? "yes" : "no"));
            GUILayout.Label("Wall sliding? " + (wallSliding ? "yes" : "no"));
        GUILayout.Label("Incline angle: " + currentPlatformIncline);
        GUILayout.EndArea();

        GUILayout.BeginArea(timeRect);
            GUI.color = Color.white;
            GUILayout.BeginHorizontal();
                if (GUILayout.Button("1x"))
                    Time.timeScale = 1;
                if (GUILayout.Button("0.75x"))
                    Time.timeScale = 0.75f;
                if (GUILayout.Button("0.5x"))
                    Time.timeScale = 0.5f;
                if (GUILayout.Button("0.25x"))
                    Time.timeScale = 0.25f;
            GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    void DrawCollisionRays()
    {
        // Left Side
        foreach(GameObject collisionPoint in leftCollisionPoints)            
            Debug.DrawRay(collisionPoint.transform.position, Vector3.left * Constants.SIDE_COLLISION_SCALE, Color.red, 0.0001f, false);

        // Right Side
        foreach (GameObject collisionPoint in rightCollisionPoints)
            Debug.DrawRay(collisionPoint.transform.position, Vector3.right * Constants.SIDE_COLLISION_SCALE, Color.red, 0.0001f, false);

        // Top
        foreach (GameObject collisionPoint in topCollisionPoints)
            Debug.DrawRay(collisionPoint.transform.position, Vector3.up * Constants.TOP_BOTTOM_COLLISION_SCALE, Color.red, 0.0001f, false);

        // Bottom
        foreach (GameObject collisionPoint in bottomCollisionPoints)
            Debug.DrawRay(collisionPoint.transform.position, Vector3.down * Constants.TOP_BOTTOM_COLLISION_SCALE, Color.red, 0.0001f, false);

        // Corners
        Debug.DrawRay(leftCornerPoint.transform.position, Vector3.left * Constants.SIDE_COLLISION_SCALE * 1.5f, Color.gray, 0.0001f, false);
        Debug.DrawRay(leftCornerPoint.transform.position, Vector3.down * Constants.TOP_BOTTOM_COLLISION_SCALE * 1.5f, Color.gray, 0.0001f, false);
        Debug.DrawRay(rightCornerPoint.transform.position, Vector3.right * Constants.SIDE_COLLISION_SCALE * 1.5f, Color.gray, 0.0001f, false);
        Debug.DrawRay(rightCornerPoint.transform.position, Vector3.down * Constants.TOP_BOTTOM_COLLISION_SCALE * 1.5f, Color.gray, 0.0001f, false);
    }

    
    #endregion
}
