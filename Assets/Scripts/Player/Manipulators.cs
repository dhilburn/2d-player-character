﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manipulators : MonoBehaviour
{
    [Tooltip("Manipulator GameObject: creates Manipulator area")]
    public GameObject manipulatorObject;
    [Tooltip("Target reticle GameObject; reticle manipulators follow")]
    public GameObject targetReticle;
    

    [Tooltip("How long it takes for the manipulator area to reach its initial growth size.")]
    public float initialGrowthRate;
    [Tooltip("How long it takes for the manipulator area to reach its final growth size.")]
    public float maxGrowthRate;
    [Tooltip("The radius for the maximum growth size.")]
    public float maxGrowthRadius;
    [Tooltip("How quickly heat and chill fire out from the player.")]
    public float fireSpeed;
    [Tooltip("How long the player has to wait between manipulator uses.")]
    public float manipCooldown;

    Vector3 initialSize = new Vector2(0.01f, 0.01f);
    Vector3 growth1 = new Vector2(1.2f, 1.2f);
    Vector3 maxGrowth;   

    int mana;

    [HideInInspector] public GameObject activeManipulatorObject;
    Manipulator activeManipulator;
    List<Manipulator> obtainedManipulators;

    bool manipulatorActive;
    bool charging = false;
    bool initialGrowthComplete = false;
    bool onCooldown;


    #region Manipulator Sprite Colors
    Vector4 noManipEquipped = new Vector4(1, 0, 1, 1);
    Vector4 heatColor = new Vector4(0.874f, 0.392f, 0.173f, 0.8f);
    Vector4 chillColor = new Vector4(0.475f, 0.725f, 0.933f, 0.8f);
    Vector4 hardenColor = new Vector4(0.5f, 0.459f, 0.008f, 0.5f);
    Vector4 softenColor = new Vector4(0.475f, 0.475f, 0.475f, 0.5f);    
    #endregion

    // Use this for initialization
    void Start () 
    {
        maxGrowth = new Vector3(maxGrowthRadius, maxGrowthRadius);
        manipulatorActive = false;
        activeManipulator = Manipulator.NONE;
        obtainedManipulators = new List<Manipulator>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if(Input.GetMouseButtonDown(2) && !(charging || onCooldown))
        {
            if (obtainedManipulators.Count > 0)
                manipulatorActive = !manipulatorActive;
            else
                manipulatorActive = false;
        }

        if (manipulatorActive)
        {
            if(Input.GetAxis("Mouse ScrollWheel") != 0 && !(charging || onCooldown))
            {
                if(Input.GetAxis("Mouse ScrollWheel") > 0)
                {
                    ChangeManipulator((int)activeManipulator, 1);
                }
                else if (Input.GetAxis("Mouse ScrollWheel") < 0)
                {
                    ChangeManipulator((int)activeManipulator, -1);
                }
            }
            // Create the manipulator
            if (Input.GetMouseButtonDown(1) && !onCooldown)
            {
                activeManipulatorObject = Instantiate(manipulatorObject, transform.position, Quaternion.identity);

                switch(activeManipulator)
                {
                    case (Manipulator.HEAT):
                        activeManipulatorObject.GetComponent<SpriteRenderer>().color = heatColor;
                        activeManipulatorObject.GetComponent<SpriteRenderer>().sortingOrder = -3;
                        break;
                    case (Manipulator.CHILL):
                        activeManipulatorObject.GetComponent<SpriteRenderer>().color = chillColor;
                        activeManipulatorObject.GetComponent<SpriteRenderer>().sortingOrder = -3;
                        break;
                    case (Manipulator.HARDEN):
                        activeManipulatorObject.GetComponent<SpriteRenderer>().color = hardenColor;
                        activeManipulatorObject.GetComponent<SpriteRenderer>().sortingOrder = 3;
                        break;
                    case (Manipulator.SOFTEN):
                        activeManipulatorObject.GetComponent<SpriteRenderer>().color = softenColor;
                        activeManipulatorObject.GetComponent<SpriteRenderer>().sortingOrder = 3;
                        break;
                    default:
                        activeManipulatorObject.GetComponent<SpriteRenderer>().color = noManipEquipped;
                        break;
                }                
                charging = true;
                StartCoroutine(ChargeManipulator(growth1, initialGrowthRate));
            }

            // Make the manipulator fire or stop
            if (Input.GetMouseButtonUp(1) && !onCooldown)
            {
                StopManipulatorCharge();
                FireManipulator();            
                StartCoroutine(ManipulatorCooldown());
            }

            // Make the Heat and Chill manipulator area follow the player
            if (Input.GetMouseButton(1) && charging)
            {
                activeManipulatorObject.transform.position = transform.position;
            }
        }
	}
    
    void StopManipulatorCharge()
    {
        StopCoroutine("ChargeManipulator");
        StopCoroutine("SpawnManipulator");        
        manipulatorObject.transform.localScale = initialSize;
        charging = false;
        initialGrowthComplete = false;
    }

    IEnumerator ChargeManipulator(Vector3 growthSize, float growthRate)
    {            
        // Set up a time step for making the area grow
        float timeStep = 0;

        // Make the area grow by lerping the scale of the manipulator area over time
        while (timeStep < growthRate)
        {
            if (Input.GetMouseButton(1))
            {
                activeManipulatorObject.transform.localScale = Vector3.Lerp(manipulatorObject.transform.localScale, growthSize, timeStep / growthRate);
                timeStep += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }
            else
                break;
        }
        // Set the size of the active manipulator and the stored object   
        if (Input.GetMouseButton(1))
        {
            activeManipulatorObject.transform.localScale = growthSize;
            manipulatorObject.transform.localScale = growthSize;
            if (!initialGrowthComplete)
            {
                initialGrowthComplete = true;
                StartCoroutine(ChargeManipulator(maxGrowth, maxGrowthRate));
            }
        }          
    }    

    

    IEnumerator ManipulatorCooldown()
    {
        Vector4 cooldownStartColor = new Vector4(0.5f, 0.5f, 0.5f, 1);
        Vector4 cooldownEndColor = new Vector4(1, 1, 1, 1);
        float timeStep = 0;

        onCooldown = true;
        GetComponent<SpriteRenderer>().color = cooldownStartColor;
        while(timeStep < manipCooldown)
        {
            GetComponent<SpriteRenderer>().color = Vector4.Lerp(cooldownStartColor, cooldownEndColor, timeStep / manipCooldown);
            yield return new WaitForEndOfFrame();
            timeStep += Time.deltaTime;
        }
        GetComponent<SpriteRenderer>().color = cooldownEndColor;
        onCooldown = false;
    }

    void FireManipulator()
    {
        try
        { 
            activeManipulatorObject.AddComponent<ManipulatorBehaviour>();
            activeManipulatorObject.GetComponent<ManipulatorBehaviour>().speed = fireSpeed;
        }
        catch(MissingReferenceException activeManipulatorDestroyed) { }
    }

    public void AddManipulator(Manipulator newManip)
    {
        obtainedManipulators.Add(newManip);
        if(activeManipulator == Manipulator.NONE)
            activeManipulator = (Manipulator)newManip;
        SortManipulators();
    }

    void SortManipulators()
    {
        if(obtainedManipulators.Count > 1)
        {
            for(int i = 0; i < obtainedManipulators.Count - 1; i++)
            {
                if((int)obtainedManipulators[i] > (int)obtainedManipulators[i + 1])
                {
                    Manipulator temp = obtainedManipulators[i + 1];
                    obtainedManipulators[i + 1] = obtainedManipulators[i];
                    obtainedManipulators[i] = temp;
                }
            }
        }
    }

    void ChangeManipulator(int currentManip, int nextVal)
    {
        if(obtainedManipulators.Count > 1)
        {
            int nextManipulatorIndex = obtainedManipulators.IndexOf((Manipulator)currentManip) + nextVal;
            Debug.Log("nextManipulatorIndex value is " + nextManipulatorIndex);

            // If next is less than the value in the first spot of the list, assign it to the last spot
            if (nextManipulatorIndex < 0)
            {
                nextManipulatorIndex = obtainedManipulators.Count - 1;
                Debug.Log("Went under; going to the last obtained manipulator: " + nextManipulatorIndex);
            }

            // If next is greater than the value in the last spot of the list, assign it to the first spot
            if (nextManipulatorIndex > obtainedManipulators.Count - 1)
            {
                nextManipulatorIndex = 0;
                Debug.Log("Went over; going to the first obtained manipulator: " + nextManipulatorIndex);
            }
            
            activeManipulator = obtainedManipulators[nextManipulatorIndex];
        }        
    }

    void OnGUI()
    {
        string activeManip = "none";

        GUILayout.BeginArea(new Rect(Screen.width - 200, 50, 200, 60));
            GUI.color = Color.black;
            GUILayout.Label(manipulatorActive ? "Manipulator active" : "Manipulator inactive");
            if(manipulatorActive)
            {
                GUILayout.BeginHorizontal();
                    GUILayout.Label("Current manipulator: ");
                    switch(activeManipulator)
                    {
                        case (Manipulator.HEAT):
                            GUI.color = Color.red;
                            activeManip = "heat";
                            break;
                        case (Manipulator.CHILL):
                            GUI.color = Color.cyan;
                            activeManip = "chill";
                            break;
                        case (Manipulator.HARDEN):
                            GUI.color = Color.yellow;
                            activeManip = "harden";
                            break;
                        case (Manipulator.SOFTEN):
                            GUI.color = Color.grey;
                            activeManip = "soften";
                            break;
                        default:
                            GUI.color = Color.magenta;
                            break;
            }
                    GUILayout.Label(activeManip);
                GUILayout.EndHorizontal();
            }
        GUILayout.EndArea();
    }
}
