﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    
    public int playerHealth;
    public int playerMana;

    public int RedKeys { get; private set; }
    public int BlueKeys { get; private set; }
    public int GoldKeys { get; private set; }

    [HideInInspector] public bool hasMasterRed;
    [HideInInspector] public bool hasMasterBlue;
    [HideInInspector] public bool hasMasterGold;

    int currentHealth;
    int currentMana;
    int maxHealth;
    int maxMana;

    

	// Use this for initialization
	void Start ()
    {
        currentHealth = maxHealth = playerHealth;
        currentMana = maxMana = playerMana;
	}

    /// <summary>
    /// AddHealth adds or subtracts from the player's health.
    /// </summary>
    /// <param name="amount">Health value to add to the player's health. Use a negative value to remove health.</param>
    /// <param name="instant">Flag for if changing the player's current or max health.</param>
    public void AddHealth(int amount, bool instant)
    {
        if (instant)
        {
            currentHealth += amount;

            if (currentHealth > maxHealth)
                currentHealth = maxHealth;
            if (currentHealth < 0)
                currentHealth = 0;
        }
        else
            maxHealth += amount;
    }

    /// <summary>
    /// AddMana adds or subtracts from the player's mana.
    /// </summary>
    /// <param name="amount">Health value to add to the player's mana. Use a negative value to remove health.</param>
    /// <param name="instant">Flag for if changing the player's current or max mana.</param>
    public void AddMana(int amount, bool instant)
    {
        if (instant)
        {
            currentMana += amount;

            if (currentMana > maxMana)
                currentMana = maxMana;
            if (currentMana < 0)
                currentMana = 0;
        }
        else
            maxMana += amount;
    }

    public void RegenHealth(int amount, int time, int healInterval)
    {

    }

    public void RegenMana(int amount, int time, int healInterval)
    {

    }

    /// <summary>
    /// AddKey either adds or removes from the player's key counts.
    /// </summary>
    /// <param name="keyColor">Key counter to add or remove from.</param>
    /// <param name="addingKey">Are you adding(true) or removing(false) a key?</param>
    public void AddKey(KeyColor keyColor, bool addingKey)
    {
        switch (keyColor)
        {
            case (KeyColor.RED):
                if (addingKey)
                    RedKeys++;
                else
                    RedKeys--;
                break;
            case (KeyColor.BLUE):
                if (addingKey)
                    BlueKeys++;
                else
                    BlueKeys--;
                break;
            case (KeyColor.GOLD):
                if (addingKey)
                    GoldKeys++;
                else
                    GoldKeys--;
                break;
        }
    }

    public void GetMasterKey(KeyColor keyColor)
    {
        switch(keyColor)
        {
            case (KeyColor.RED):
                hasMasterRed = true;
                break;
            case (KeyColor.BLUE):
                hasMasterBlue = true;
                break;
            case (KeyColor.GOLD):
                hasMasterGold = true;
                break;
        }
    }

    void OnGUI()
    {
        Rect debugInfoRect = new Rect(0, 250, 200, 70);
        GUILayout.BeginArea(debugInfoRect);
            GUILayout.Label(hasMasterRed? "Carrying A red key" : "Carrying " + RedKeys + " red keys" );
            GUILayout.Label(hasMasterBlue? "Carrying A blue key" :  "Carrying " + BlueKeys + " blue keys");
            GUILayout.Label(hasMasterGold? "Carrying A gold key" : "Carrying " + GoldKeys + " gold keys");
        GUILayout.EndArea();
    }
}
