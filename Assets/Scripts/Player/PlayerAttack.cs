﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [Tooltip("The player weapon object.")]
    public GameObject chakram;
    [Tooltip("How much damage the player's weapon deals.")]
    public float damage = 5f;    
    [Tooltip("How quickly the player's weapon moves.")]
    public float speed = 2f;
    public float range = 3f;
    [Tooltip("Wait time between weapon throws.")]
    public float throwCooldown = 1;
    [Tooltip("How many weapons can be on screen at once.")]
    public int maxActiveChakram = 3;

    public bool useReticle;

    bool facingRight;

    [HideInInspector] public int activeChakram;
    [HideInInspector] public bool throwingChakram;

    Vector3 reticlePos;

    Vector3 topPlayerPos,
            bottomPlayerPos,
            leftPlayerPos,
            rightPlayerPos,
            playerPos;


	// Use this for initialization
	void Start ()
    {
        activeChakram = 0;
        facingRight = GetComponent<PlayerPhysics2D>().facingRight;
        UpdatePositions();
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePositions();
        facingRight = GetComponent<PlayerPhysics2D>().facingRight;
        if (Input.GetMouseButtonDown(0) && !throwingChakram && activeChakram < maxActiveChakram)
        {
            throwingChakram = true;
            StartCoroutine("ThrowChakram");
            activeChakram++;
        }
	}

    void UpdatePositions()
    {
        playerPos = transform.position;

        if (useReticle)
        {
            topPlayerPos = GetComponent<PlayerPhysics2D>().topCollisionPoints[0].transform.position;
            bottomPlayerPos = GetComponent<PlayerPhysics2D>().bottomCollisionPoints[0].transform.position;
            leftPlayerPos = GetComponent<PlayerPhysics2D>().leftCollisionPoints[0].transform.position;
            rightPlayerPos = GetComponent<PlayerPhysics2D>().rightCollisionPoints[0].transform.position;
            reticlePos = GameObject.FindGameObjectWithTag("Target Reticle").transform.position;
        }
    }

    IEnumerator ThrowChakram()
    {
        Vector3 spawnPos = playerPos;

        if (useReticle)
        {
            // Check if the reticle is out past the right player pos.
            // If it is, the spawn pos is on the RIGHT
            if (reticlePos.x > rightPlayerPos.x)
            {
                spawnPos = spawnPos + new Vector3((transform.localScale.x / 2) + (chakram.transform.localScale.x / 2) + (transform.localScale.x * 0.008f), 0);
            }
            // Check if the reticle is out past the left player pos.
            // If it is, the spawn pos is on the LEFT
            else if (reticlePos.x < leftPlayerPos.x)
            {
                spawnPos = spawnPos - new Vector3((transform.localScale.x / 2) + (chakram.transform.localScale.x / 2) + (transform.localScale.x * 0.008f), 0);
            }
            // Check if the reticle is between the left and right points
            else if (reticlePos.x < rightPlayerPos.x && reticlePos.x > leftPlayerPos.x)
            {
                // Check if the reticle is out past the top player pos
                // If it is, the spawn pos is on TOP
                if (reticlePos.y > topPlayerPos.y)
                {
                    spawnPos = spawnPos + new Vector3(0, (transform.localScale.y / 2) + (chakram.transform.localScale.y / 2) + (transform.localScale.y * 0.008f));
                }
                // Check if the reticle  is out past the bottom point
                // If it is, the spawn pos is on the BOTTOM
                else if (reticlePos.y < bottomPlayerPos.y)
                {
                    spawnPos = spawnPos - new Vector3(0, (transform.localScale.y / 2) + (chakram.transform.localScale.y / 2) + (transform.localScale.y * 0.008f));
                }
            }
        }
        else
        {
            if(facingRight)
                spawnPos = spawnPos + new Vector3((transform.localScale.x / 2) + (chakram.transform.localScale.x / 2) + (transform.localScale.x * 0.008f), 0);
            else
                spawnPos = spawnPos - new Vector3((transform.localScale.x / 2) + (chakram.transform.localScale.x / 2) + (transform.localScale.x * 0.008f), 0);
        }     

        GameObject instantChakram = Instantiate(chakram, spawnPos, Quaternion.identity) as GameObject;
        yield return new WaitForSeconds(throwCooldown);
        throwingChakram = false;
    }
}
