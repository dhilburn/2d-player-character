﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManipulatorBehaviour : MonoBehaviour
{
    [HideInInspector] public float damage;
    public float speed;

    bool useReticle;
    bool playerFacingRight;
    GameObject player;
    Vector3 spawnPos;

    Vector3 reticlePos;
    Vector3 moveDirection;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.Find("Player");
        playerFacingRight = player.GetComponent<PlayerPhysics2D>().facingRight;
        useReticle = player.GetComponent<PlayerAttack>().useReticle;
        spawnPos = player.transform.position;
        if(player.GetComponent<PlayerAttack>().useReticle)
            reticlePos = GameObject.FindGameObjectWithTag("Target Reticle").transform.position;        
        transform.localScale = player.GetComponent<Manipulators>().activeManipulatorObject.transform.localScale;
	}
	
	// Update is called once per frame
	void FixedUpdate()
    {
        MoveManipulator();
        if (DetectCollision())
            Destroy(gameObject);
    }

    

    void MoveManipulator()
    {
        Vector3 move = Vector3.zero; 

        if (useReticle)
        {
            move = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            move.z = 0;            
        }
        else
        {
            if(playerFacingRight)
            {
                move = Vector3.right;
            }
            else
            {
                move = Vector3.left;
            }
        }

        transform.Translate(Vector3.Normalize(move) * Time.deltaTime * speed);
    }

    bool DetectCollision()
    {
        return Physics2D.Raycast(transform.position, Vector3.zero, 0.0001f, 1 << LayerMask.NameToLayer(Constants.ENVIRONMENT)).transform != null;
    }
}
