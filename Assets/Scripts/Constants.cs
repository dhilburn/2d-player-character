﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickupType
{
    HEALTH_INST,
    HEALTH_PERM,
    MANA_INST,
    MANA_PERM,
    KEY_INST,
    KEY_MASTR,
    ABILITY_DOUBLE,
    ABILITY_CLIMB,
    MANIPULATOR
}

public enum KeyColor
{
    RED,
    BLUE,
    GOLD
}

public enum Manipulator
{
    NONE,
    HEAT,
    CHILL,
    HARDEN,
    SOFTEN
}

public class Constants
{
    public const string PICKUP = "Pickup";
    public const string COLLISION_POINT = "Collision Point";
    public const string PLAYER = "Player";
    public const string ENEMY = "Enemy";
    public const string ENVIRONMENT = "Environment";
    public const string SEMISOLID = "Semisolid";

    public const float SIDE_COLLISION_SCALE = 0.06f;
    public const float TOP_BOTTOM_COLLISION_SCALE = 0.015f;

}
