﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class SemisolidBehaviour : MonoBehaviour
{
    BoxCollider2D platformCollider;
    GameObject[] leftCollisionPoints = new GameObject[11];
    GameObject[] rightCollisionPoints = new GameObject[11];
    GameObject[] bottomCollisionPoints = new GameObject[101];
    GameObject[] topCollisionPoints = new GameObject[101];

    float initialX,
          initialY,
          finalX,
          finalY,
          magnitudeX,
          magnitudeY;

	// Use this for initialization
	void Start ()
    {
        platformCollider = GetComponent<BoxCollider2D>();        
        SetCollisionBounds();
        SetBounds();
        CreateCollisionPoints();
	}
	
	// Update is called once per frame
	void Update ()
    {
        DrawCollisionRays();
	}

    void FixedUpdate()
    {
        if (DetectPlayerCollision())
            GetComponent<BoxCollider2D>().enabled = false;
        else
            GetComponent<BoxCollider2D>().enabled = true;
    }

    void SetCollisionBounds()
    {
        platformCollider.offset = new Vector2(0, 0.25f);
        platformCollider.size = new Vector2(1, 0.5f);
    }

    void SetBounds()
    {
        initialX = platformCollider.bounds.min.x;
        initialY = platformCollider.bounds.min.y;
        finalX = platformCollider.bounds.max.x;
        finalY = platformCollider.bounds.max.y;
        magnitudeX = platformCollider.bounds.size.x;
        magnitudeY = platformCollider.bounds.size.y;
    }

    #region Collision Point Creation Functions
    
    void CreateCollisionPoints()
    {
        CreateTopCollisionPoints();
        CreateBottomCollisionPoints();
        CreateLeftCollisionPoints();
        CreateRightCollisionPoints();
    }

    void CreateTopCollisionPoints()
    {
        for (int i = 0; i < 101; i++)
        {
            topCollisionPoints[i] = new GameObject();
        }

        topCollisionPoints[0].transform.position = new Vector3(initialX, platformCollider.bounds.max.y);
        for (int i = 1; i < 100; i++)
        {
            topCollisionPoints[i].transform.position = new Vector3(initialX + (magnitudeX * (0.01f * i)), platformCollider.bounds.max.y);
        }
        topCollisionPoints[100].transform.position = new Vector3(finalX, platformCollider.bounds.max.y);

        foreach (GameObject collisionPoint in topCollisionPoints)
        {
            collisionPoint.transform.parent = transform;
            collisionPoint.gameObject.name = Constants.COLLISION_POINT;
            collisionPoint.gameObject.tag = Constants.COLLISION_POINT;
            collisionPoint.gameObject.layer = LayerMask.NameToLayer(Constants.ENVIRONMENT);
        }
    }

    void CreateBottomCollisionPoints()
    {

        for(int i = 0; i < 101; i++)
        {
            bottomCollisionPoints[i] = new GameObject();
        }

        bottomCollisionPoints[0].transform.position = new Vector3(initialX, platformCollider.bounds.min.y);
        for(int i = 1; i < 100; i++)
        {
            bottomCollisionPoints[i].transform.position = new Vector3(initialX + (magnitudeX * (0.01f * i)), platformCollider.bounds.min.y);
        }
        bottomCollisionPoints[100].transform.position = new Vector3(finalX, platformCollider.bounds.min.y);

        foreach(GameObject collisionPoint in bottomCollisionPoints)
        {
            collisionPoint.transform.parent = transform;
            collisionPoint.gameObject.name = Constants.COLLISION_POINT;
            collisionPoint.gameObject.tag = Constants.COLLISION_POINT;
            collisionPoint.gameObject.layer = LayerMask.NameToLayer(Constants.ENVIRONMENT);
        }
    }

    void CreateLeftCollisionPoints()
    {

        for (int i = 0; i < 11; i++)
            leftCollisionPoints[i] = new GameObject();

        leftCollisionPoints[0].transform.position = new Vector3(platformCollider.bounds.min.x, initialY);
        for (int i = 1; i < 10; i++)
            leftCollisionPoints[i].transform.position = new Vector3(platformCollider.bounds.min.x, initialY + (magnitudeY * (0.1f * i)));
        leftCollisionPoints[10].transform.position = new Vector3(platformCollider.bounds.min.x, finalY);

        foreach(GameObject collisionPoint in leftCollisionPoints)
        {
            collisionPoint.transform.parent = transform;
            collisionPoint.gameObject.name = Constants.COLLISION_POINT;
            collisionPoint.gameObject.tag = Constants.COLLISION_POINT;
            collisionPoint.gameObject.layer = LayerMask.NameToLayer(Constants.ENVIRONMENT);
        }
    }

    void CreateRightCollisionPoints()
    {

        for (int i = 0; i < 11; i++)
            rightCollisionPoints[i] = new GameObject();

        rightCollisionPoints[0].transform.position = new Vector3(platformCollider.bounds.max.x, initialY);
        for (int i = 1; i < 10; i++)
            rightCollisionPoints[i].transform.position = new Vector3(platformCollider.bounds.max.x, initialY + (magnitudeY * (0.1f * i)));
        rightCollisionPoints[10].transform.position = new Vector3(platformCollider.bounds.max.x, finalY);

        foreach (GameObject collisionPoint in rightCollisionPoints)
        {
            collisionPoint.transform.parent = transform;
            collisionPoint.gameObject.name = Constants.COLLISION_POINT;
            collisionPoint.gameObject.tag = Constants.COLLISION_POINT;
            collisionPoint.gameObject.layer = LayerMask.NameToLayer(Constants.ENVIRONMENT);
        }
    }

    #endregion

    #region Detect Collision functions

    bool DetectPlayerCollision()
    {
        return DetectBottomCollision()
            || DetectLeftCollision()
            || DetectRightCollision()
            && !DetectTopCollision();
    }

    bool DetectBottomCollision()
    {
        bool colliding = false;

        for(int i = 0; i < bottomCollisionPoints.Length; i++)
        {
            if(Physics2D.Raycast(bottomCollisionPoints[i].transform.position, Vector3.down, Constants.TOP_BOTTOM_COLLISION_SCALE, 1 << LayerMask.NameToLayer(Constants.PLAYER)).transform != null
            || Physics2D.Raycast(bottomCollisionPoints[i].transform.position, Vector3.up, magnitudeY * 0.33f, 1 << LayerMask.NameToLayer(Constants.PLAYER)).transform != null)
            {
                colliding = true;
                //Debug.Log("Player collided underneath me");
                break;
            }
        }

        return colliding;
    }

    bool DetectLeftCollision()
    {
        bool colliding = false;

        for(int i = 0; i < leftCollisionPoints.Length; i++)
        {
            if(Physics2D.Raycast(leftCollisionPoints[i].transform.position, Vector3.left, Constants.SIDE_COLLISION_SCALE, 1 << LayerMask.NameToLayer(Constants.PLAYER)).transform != null)
            {
                colliding = true;
                //Debug.Log("Player collided on my left side");
                break;
            }
        }

        return colliding;
    }

    bool DetectRightCollision()
    {
        bool colliding = false;

        for (int i = 0; i < rightCollisionPoints.Length; i++)
        {
            if (Physics2D.Raycast(rightCollisionPoints[i].transform.position, Vector3.right, Constants.SIDE_COLLISION_SCALE, 1 << LayerMask.NameToLayer(Constants.PLAYER)).transform != null)
            {
                colliding = true;
                //Debug.Log("Player collided on my right side");
                break;
            }
        }
        

        return colliding;
    }

    bool DetectTopCollision()
    {
        bool colliding = false;

        for(int i = 0; i < topCollisionPoints.Length; i++)
        {
            if(Physics2D.Raycast(topCollisionPoints[i].transform.position, Vector3.up, GameObject.Find("Player").GetComponent<BoxCollider2D>().bounds.size.y / 2, 1 << LayerMask.NameToLayer(Constants.PLAYER)).transform != null)
            {
                colliding = true;
                //Debug.Log("Player is on top of me.");
                break;
            }
        }

        return colliding;
    }

    #endregion

    #region Collision Ray Debug functions

    void DrawCollisionRays()
    {
        DrawTopCollisionRays();
        DrawBottomCollisionRays();
        DrawInnerCollisionRays();
        DrawLeftCollisionRays();
        DrawRightCollisionRays();

    }

    void DrawTopCollisionRays()
    {
        foreach (GameObject collisionPoint in topCollisionPoints)
            Debug.DrawRay(collisionPoint.transform.position, Vector3.up * GameObject.Find("Player").GetComponent<BoxCollider2D>().bounds.size.y / 2, Color.yellow, 0.0001f, false);
    }

    void DrawBottomCollisionRays()
    {
        foreach (GameObject collisionPoint in bottomCollisionPoints)
            Debug.DrawRay(collisionPoint.transform.position, Vector3.down * Constants.TOP_BOTTOM_COLLISION_SCALE, Color.yellow, 0.0001f, false);
    }

    void DrawLeftCollisionRays()
    {
        foreach (GameObject collisionPoint in leftCollisionPoints)
            Debug.DrawRay(collisionPoint.transform.position, Vector3.left * Constants.SIDE_COLLISION_SCALE, Color.yellow, 0.0001f, false);
    }

    void DrawRightCollisionRays()
    {
        foreach (GameObject collisionPoint in rightCollisionPoints)
            Debug.DrawRay(collisionPoint.transform.position, Vector3.right * Constants.SIDE_COLLISION_SCALE, Color.yellow, 0.0001f, false);
    }

    void DrawInnerCollisionRays()
    {
        foreach (GameObject collisionPoint in bottomCollisionPoints)
            Debug.DrawRay(collisionPoint.transform.position, Vector3.up * magnitudeY * 0.33f, Color.yellow, 0.0001f, false);
    }

    #endregion
}
