﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(BoxCollider2D))]
public class DoorBehaviour : MonoBehaviour 
{
    public KeyColor unlockColor;

    GameObject player;

	// Use this for initialization
	void Start () 
    {
        player = GameObject.Find("Player");
        switch (unlockColor)
        {
            case(KeyColor.RED):
                GetComponent<SpriteRenderer>().color = Color.red;
                break;
            case (KeyColor.BLUE):
                GetComponent<SpriteRenderer>().color = Color.blue;
                break;
            case (KeyColor.GOLD):
                GetComponent<SpriteRenderer>().color = Color.yellow;
                break;
        }
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag.Equals(Constants.PLAYER))
        {
            switch (unlockColor)
            {
                case(KeyColor.RED):
                    if (player.GetComponent<PlayerController>().RedKeys > 0 || player.GetComponent<PlayerController>().hasMasterRed)
                    {
                        Destroy(gameObject);
                        if(!player.GetComponent<PlayerController>().hasMasterRed)
                            player.GetComponent<PlayerController>().AddKey(KeyColor.RED, false);
                    }
                    break;
                case (KeyColor.BLUE):
                    if (player.GetComponent<PlayerController>().BlueKeys > 0 || player.GetComponent<PlayerController>().hasMasterBlue)
                    {
                        Destroy(gameObject);
                        if(!player.GetComponent<PlayerController>().hasMasterBlue)
                            player.GetComponent<PlayerController>().AddKey(KeyColor.BLUE, false);
                    }
                    break;
                case (KeyColor.GOLD):
                    if (player.GetComponent<PlayerController>().GoldKeys > 0 || player.GetComponent<PlayerController>().hasMasterGold)
                    {
                        Destroy(gameObject);
                        if(!player.GetComponent<PlayerController>().hasMasterGold)
                            player.GetComponent<PlayerController>().AddKey(KeyColor.GOLD, false);
                    }
                    break;
            }
        }
    }
}
