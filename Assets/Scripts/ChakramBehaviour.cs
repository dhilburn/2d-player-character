﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChakramBehaviour : MonoBehaviour
{

    [HideInInspector] public float damage;
    float range;
    float speed;

    bool facingRight;
    bool boomeranging;
    Vector3 spawnPos;
    GameObject player;

    bool useReticle;
    Vector3 reticlePos;
    Vector3 endPos;
    Vector3 endPosNormal;
    

    float timeStep;
    float boomerangTimeStep;

	// Use this for initialization
	void Start ()
    {
        player = GameObject.Find(Constants.PLAYER);          
        spawnPos = transform.position;
        facingRight = player.GetComponent<PlayerPhysics2D>().facingRight;
        damage = player.GetComponent<PlayerAttack>().damage;
        speed = player.GetComponent<PlayerAttack>().speed;
        boomeranging = false;
        timeStep = 0;
        boomerangTimeStep = 0;

        useReticle = player.GetComponent<PlayerAttack>().useReticle;

        if (useReticle)
        {
            range = Vector3.Distance(player.transform.position, reticlePos);
            reticlePos = GameObject.FindGameObjectWithTag("Target Reticle").transform.position;
            endPos = reticlePos;
            endPosNormal = Vector3.Normalize(endPos);
        }
        else
        {
            range = player.GetComponent<PlayerAttack>().range;
        }
        Debug.Log("EndPos is " + endPos);       
    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        if (!boomeranging)
            timeStep += Time.deltaTime * speed / range;

        if(useReticle)
            transform.position = Vector3.Lerp(transform.position, GameObject.FindGameObjectWithTag("Target Reticle").transform.position, timeStep);
        else
        {
            if(facingRight)
                transform.position += new Vector3(Time.deltaTime * speed * range, 0, 0);
            else
                transform.position -= new Vector3(Time.deltaTime * speed * range, 0, 0);
        }
        


        // Check if the chakram is out past its range and start boomeranging if it is
        if (useReticle)
        {
            if (Vector3.Distance(transform.position, GameObject.FindGameObjectWithTag("Target Reticle").transform.position) < 0.05f)
            {
                boomeranging = true;
            }
        }
        else
        {
            if ((transform.position - spawnPos).magnitude > range && !boomeranging)
            {
                boomeranging = true;
            }
        }

        // Make the chakram boomerang back toward the player
        if (boomeranging)
        {
            boomerangTimeStep += Time.deltaTime * speed / range;
            transform.position = Vector3.Lerp(transform.position, player.transform.position, boomerangTimeStep);
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag.Equals(Constants.PLAYER))
        {
            Destroy(gameObject);
            player.GetComponent<PlayerAttack>().activeChakram--;
        }        
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.tag.Equals(Constants.PICKUP))
            other.gameObject.transform.position = transform.position;
    }
}

